<?php

use Faker\Generator as Faker;

$factory->define(App\Subject::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->randomElement([
            'Programming in C',
            'Discrete math',
            'Circuits',
            'Basic Economy',
            'Industrial production',
            'Mathematics',
            'Basic Business',
            'Advance Algorithm',
            'Artificial Intelligence',
            'Basic Biology',
            'Design',
            'Web Development',
            'Structure',
            'Social Studies',
            'Geological Studies',
            'Anatomy',
            'Fabric',
            'Production Management',
            'Electric & Electronic Design'
            ]),
        'code'=>$faker->unique()->randomElement([
            '001',
            '002',
            '003',
            '004',
            '005',
            '006',
            '007',
            '008',
            '009',
            '010',
            '011',
            '012',
            '013',
            '014',
            '015',
            '016',
            '017',
            '018',
            '019'
            ]),
        'department_id'=>$faker->randomElement([
            '01',
            '01',
            '02',
            '09',
            '05',
            '06',
            '08',
            '01',
            '01',
            '10',
            '03',
            '01',
            '04',
            '12',
            '11',
            '10',
            '07',
            '05',
            '02'
            ]),
    ];
});