<?php

use Faker\Generator as Faker;

$factory->define(App\Department::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->randomElement([
                'CSE',
                'EEE',
                'ARCH',
                'CE',
                'IPE',
                'F&A',
                'TEX',
                'BBA',
                'ECO',
                'BIO',
                'GEO',
                'SOC'
                ]),
        'code'=>$faker->unique()->randomElement([
                '0101',
                '0102',
                '0103',
                '0104',
                '0105',
                '0106',
                '0107',
                '0108',
                '0109',
                '0110',
                '0111',
                '0112'
                ]),
    ];
});
