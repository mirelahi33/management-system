<?php

use Faker\Generator as Faker;

$factory->define(App\Year::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->randomElement([
            '18-19',
            '19-20',
            '20-21',
            '21-22',
            '22-23'
            ]),
        'code'=>$faker->randomElement([
            '1819',
            '1920',
            '2021',
            '2122',
            '2223'
            ]),
        'starting_year'=>$faker->randomElement([
            '2018',
            '2019',
            '2020',
            '2021',
            '2022'
            ]),
        'ending_year'=>$faker->randomElement([
            '2019',
            '2020',
            '2021',
            '2022',
            '2023'
            ]),
    ];
});
