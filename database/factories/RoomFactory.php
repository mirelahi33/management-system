<?php

use Faker\Generator as Faker;

$factory->define(App\Room::class, function (Faker $faker) {
    return [
        'code'=>$faker->unique()->randomElement([
            '7A01',
            '7B03',
            '7C02',
            '6A04',
            '6B07',
            '5A02',
            '5C02',
            '5B05',
            '5A04'
            ]),
    'floor'=>$faker->randomElement([
            '07',
            '07',
            '07',
            '06',
            '06',
            '05',
            '05',
            '05',
            '05'
            ]),
    ];
});
