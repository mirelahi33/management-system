<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Room;
use App\Http\Resources\RoomCollection;
use App\Http\Resources\RoomResource;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $allRoom=Room::orderBy('id','DESC')->paginate(10);
        // return Department::latest()->paginate(10);

        return new RoomCollection($allRoom);
    }

    public function search($field,$query)
    {
        return new RoomCollection(Room::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code'=>'required|string|max:191|unique:rooms',
            'floor'=>'required|string|max:191',
        ]);

        return Room::create([
            'code'=>$request['code'],
            'floor'=>$request['floor'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room = Room::findOrFail($id);

        $this->validate($request,[
            'code'=>'required|string|max:191|unique:rooms,code,'.$id,
            'floor'=>'required|string|max:191',

        ]);

        $room->update($request->all());
        return ['message' => 'Updated the room info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::findOrFail($id);
        // delete the user

        $room->delete();

        return ['message' => 'Department Deleted'];
    }
}
