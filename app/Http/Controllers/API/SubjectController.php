<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use App\Department;
use App\Http\Resources\SubjectCollection;
use App\Http\Resources\SubjectResource;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
	{
       $SubjectData=Subject::orderBy('id','DESC')->paginate(10);
        $SubjectData->load('department');
      
        return new SubjectCollection($SubjectData);
    }
    public function search($query_field,$query)
    {   if($query_field=='department'){
            $dept = Subject::with('department')->whereHas('department', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new SubjectCollection($dept);
        }
        else {
            return new SubjectCollection(Subject::where($query_field,'LIKE',"%$query%")->latest()->paginate(10));
        } 
       
    //    $data= DB::table('subjects')->join('departments','departments.id','subjects.department_id')->where('departments.name','LIKE',"%$query%")->get();
       
    //    return $data;
        
        // return new SubjectCollection(Subject::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:subjects',

        ]);

        return Subject::create([
            'name'=>$request['name'],
            'code'=>$request['code'],
            'department_id'=>$request['department_id'],
           

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = Subject::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:subjects,code,'.$id,

        ]);

        $subject->update($request->all());
        return ['message' => 'Updated the subject info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::findOrFail($id);
        // delete the user

        $subject->delete();

        return ['message' => 'Subject Deleted'];
    }
}
