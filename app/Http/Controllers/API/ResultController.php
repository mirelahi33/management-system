<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use App\Department;
use App\Student;
use App\Semester;
use App\Section;
use App\Year;
use App\Exam;
use App\Result;
use App\Http\Resources\ResultCollection;
use App\Http\Resources\ResultResource;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $dept = Exam::with('subject')->whereHas('subject', function($q) {
        //     $q->with('name');
        //     })->get();
        $ResultData=Result::orderBy('id','DESC')->paginate(10);
        $ResultData->load('exam.subject','exam.department','exam.semester','exam.section','exam.year');
        // $ResultData->load('student');

        return new ResultCollection($ResultData);

        // return $ResultData;        
    }

    public function search($query_field,$query)
    {   if($query_field=='department'){
            $dept = Result::with('exam.department','exam.subject','exam.semester','exam.section','exam.year')->whereHas('exam.department', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ResultCollection($dept);
        }
        elseif($query_field=='exam'){
            $exam = Result::with('exam','exam.subject','exam.department','exam.semester','exam.section','exam.year')->whereHas('exam', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ResultCollection($exam);
        }
        elseif($query_field=='student'){
            $student = Result::with('student','exam.subject','exam.department','exam.semester','exam.section','exam.year')->whereHas('student', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ResultCollection($student);
        }
        elseif($query_field=='subject'){
            $subject = Result::with('exam.subject','exam.department','exam.semester','exam.section','exam.year')->whereHas('exam.subject', function($q) use ($query){
                $q->where('name','LIKE',"%$query%");
                })->get(); 

            // $subject->load('exam.semester');
            // return $subject;
            return new ResultCollection($subject);
        }
        elseif($query_field=='section'){
            $section = Result::with('exam.section','exam.subject','exam.department','exam.semester','exam.year')->whereHas('exam.section', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ResultCollection($section);
        }
        elseif($query_field=='semester'){
            $semester = Result::with('exam.semester','exam.section','exam.subject','exam.department','exam.year')->whereHas('exam.semester', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ResultCollection($semester);
        }
        elseif($query_field=='year'){
            $year = Result::with('exam.year','exam.semester','exam.section','exam.subject','exam.department')->whereHas('exam.year', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ResultCollection($year);
        }
        else {
            $ResultData=Result::where($query_field,'LIKE',"%$query%")->latest()->paginate(10);
            $ResultData->load('exam.subject','exam.department','exam.semester','exam.section','exam.year');
            return new ResultCollection($ResultData);
        } 
       
    //    $data= DB::table('subjects')->join('departments','departments.id','subjects.department_id')->where('departments.name','LIKE',"%$query%")->get();
       
    //    return $data;
        
        // return new SubjectCollection(Subject::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'score'=>'required|string|max:191',

        ]);

        return Result::create([
            'score'=>$request['score'],
            'exam_id'=>$request['exam_id'],
            'student_id'=>$request['student_id'],
           
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = Result::findOrFail($id);

        $this->validate($request,[
            'score'=>'required|string|max:191',

        ]);

        $result->update($request->all());
        return ['message' => 'Updated the result'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Result::findOrFail($id);
        // delete the user

        $result->delete();

        return ['message' => 'Result Deleted'];
    }
}
