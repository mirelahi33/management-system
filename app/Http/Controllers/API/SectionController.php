<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Section;
use App\Http\Resources\SectionCollection;
use App\Http\Resources\SectionResource;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $allSection=Section::orderBy('id','DESC')->paginate(10);
        // return Department::latest()->paginate(10);

        return new SectionCollection($allSection);
    }

    public function search($field,$query)
    {
        return new SectionCollection(Section::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:sections',
        ]);

        return Section::create([
            'name'=>$request['name'],
            'code'=>$request['code'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section = Section::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:sections,code,'.$id,

        ]);

        $section->update($request->all());
        return ['message' => 'Updated the section info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::findOrFail($id);
        // delete the user

        $section->delete();

        return ['message' => 'Section Deleted'];
    }
}
