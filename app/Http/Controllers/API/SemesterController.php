<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Semester;
use App\Http\Resources\SemesterCollection;
use App\Http\Resources\SemesterResource;

class SemesterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $allSemester=Semester::orderBy('id','DESC')->paginate(10);
        // return Department::latest()->paginate(10);

        return new SemesterCollection($allSemester);
    }

    public function search($field,$query)
    {
        return new SemesterCollection(Semester::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:semesters',

        ]);

        return Semester::create([
            'name'=>$request['name'],
            'code'=>$request['code'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $semester = Semester::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:semesters,code,'.$id,

        ]);

        $semester->update($request->all());
        return ['message' => 'Updated the semester info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $semester = Semester::findOrFail($id);
        // delete the user

        $semester->delete();

        return ['message' => 'Semester Deleted'];
    }
}
