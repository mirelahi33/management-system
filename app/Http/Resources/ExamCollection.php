<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ExamCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($exam){
                return [
                  
                    'id' => $exam->id,
                    'name' => $exam->name,
                    'code' => $exam->code,
                    'date' => $exam->date,
                    'department'=>$exam['department'],
                    'room'=>$exam['room'],
                    'subject'=>$exam['subject'],
                    'section'=>$exam['section'],
                    'semester'=>$exam['semester'],
                    'year'=>$exam['year']
                ];
            })
        ];
    }
}
